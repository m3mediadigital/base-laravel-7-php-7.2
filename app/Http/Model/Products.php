<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Products extends Model
{
    use Sluggable;
    
    protected $table = 'products';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files');
    }

    public function pages()
    {
        return $this->belongsTo('App\Http\Model\Pages');
    }

    public function contents()
    {
        return $this->hasMany('App\Http\Model\ContentProducts');
    }

    public function category_products()
    {
        return $this->belongsTo('App\Http\Model\CategoryProducts');
    }

    public function setData(&$product, $request)
    {
        $product->title = $request['title'];
        $product->subtitle = $request['subtitle'];
        $product->content = $request['content'];
        $product->category_products_id = $product->category_products_id ? $product->category_products_id : $request['category_products_id'];
    }
}
