<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Maps extends Model
{
    protected $table = 'maps';

    protected $fillable = ['title','maps'];
}