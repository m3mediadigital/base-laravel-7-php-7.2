<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Pages extends Model
{
    use Sluggable;
    
    protected $table = 'pages';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files');
    }

    public function products()
    {
        return $this->hasMany('App\Http\Model\Products');
    }

    public function category_products()
    {
        return $this->hasMany('App\Http\Model\CategoryProducts');
    }

    public function category_pages()
    {
        return $this->belongsTo('App\Http\Model\CategoryPages');
    }

     public function posts()
    {
        return $this->hasMany('App\Http\Model\Posts');
    }

    public function setData(&$page, $request)
    {
        $page->title = $request['title'];
        $page->subtitle = isset($request['subtitle']) ? $request['subtitle'] : '';
        $page->content_top = $request['content_top'];
        $page->content_bottom = isset($request['content_bottom']) ? $request['content_bottom'] : '';
    }
}
