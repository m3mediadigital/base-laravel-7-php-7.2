<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Settings extends Model
{
    use Sluggable;
    
    protected $table = 'settings';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function setData(&$setting, $request)
    {
        $setting->title = $request['title'];
        $setting->content = $request['content'];
    }
}
