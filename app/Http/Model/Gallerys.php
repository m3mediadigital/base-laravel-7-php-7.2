<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Gallerys extends Model
{
    protected $table = 'gallerys';

    public function files()
    {
        return $this->hasMany('App\Http\Model\Files');
    }
}
