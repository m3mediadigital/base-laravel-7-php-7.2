<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Slides extends Model
{
    protected $table = 'slides';

    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files');
    }

    public function setData(&$slide, $request)
    {
        $slide->link = $request['link'];
        $slide->position = $request['position'];
        $slide->active = $request['active'];
        $slide->start_at =  date('Y-m-d', strtotime($request['start_at']));
        $slide->finish_at =date('Y-m-d', strtotime($request['finish_at']));
    }
}
