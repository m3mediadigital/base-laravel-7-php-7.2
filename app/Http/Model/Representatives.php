<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Representatives extends Authenticatable
{
    protected $table = 'representatives';

    protected $guards = 'representatives';

    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password',
    ];


    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files');
    }

    public function setData(&$contact, $request)
    {
        $contact->name = $request['name'];
        $contact->phone = $request['phone'];
        $contact->email = $request['email'];
        $contact->cpf = $request['cpf'];
        $contact->city = $request['city'];
        $contact->companies = isset($request['companies']) ? $request['companies'] : $contact->companies;
        $contact->social = isset($request['razao']) ? $request['razao'] : $contact->social;
        $contact->contact = $request['contact'];
        $contact->cnpj = $request['cnpj'];
        $contact->n_core_pj = $request['n_core_pj'];
        $contact->cidade = $request['cidade'];
        $contact->read = 0;
    }
}
