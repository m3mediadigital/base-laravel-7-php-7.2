<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CategoryProducts extends Model
{
    use Sluggable;

    protected $table = 'category_products';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function products()
    {
        return $this->hasMany('App\Http\Model\Products');
    }

    public function pages()
    {
        return $this->belongsTo('App\Http\Model\Pages');
    }

    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files')->select('filename');
    }

    public function setData(&$content, $request)
    {
        $content->title = $request['title'];
        $content->position = $request['position'];
    }
}
