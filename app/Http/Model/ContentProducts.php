<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ContentProducts extends Model
{
    use Sluggable;

    protected $table = 'content_products';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function products()
    {
        return $this->belongsTo('App\Http\Model\Products','products_id','id');
    }

    public function setData(&$content, $request)
    {
        $content->title = $request['title'];
        $content->content = $request['content'];
        $content->products_id = $request['products_id'];
    }
}
