<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Posts extends Model
{
    use Sluggable;
    
    protected $table = 'posts';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Http\Model\Files');
    }

    public function pages()
    {
        return $this->belongsTo('App\Http\Model\Pages');
    }

    public function gallerys()
    {
        return $this->hasMany('App\Http\Model\Gallerys','id', 'gallerys_id');
    }


    public function setData(&$post, $request)
    {
        $post->title = $request['title'];
        $post->subtitle = $request['subtitle'];
        $post->link = $request['link'];
        $post->content = $request['content'];
        $post->pages_id = $request['pages_id'];
    }
}
