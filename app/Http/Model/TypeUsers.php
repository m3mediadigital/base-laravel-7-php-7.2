<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use App\Traits\UuidGenerate;

class TypeUsers extends Model
{
    
    protected $table = 'type_users';
    protected $fillable = ['id','title'];
}
