<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';
    protected $filleble = ['type','name','phone','email','subject','message'];

    public function setData(&$contact, $request)
    {
        $contact->name = $request['name'];
        $contact->phone = $request['phone'];
        $contact->email = $request['email'];
        $contact->city = $request['city'];
        $contact->cargo = $request['cargo'];
        $contact->message = $request['message'];
        $contact->read = 0;
    }
}
