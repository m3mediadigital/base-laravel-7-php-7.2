<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class Files extends Model
{
    protected $table = 'files';
    
    public function products()
    {
        return $this->hasMany('App\Http\Model\Products');
    }

     public function gallerys()
    {
        return $this->belonsTo('App\Http\Model\Gallerys');
    }

    public function posts()
    {
        return $this->hasMany('App\Http\Model\Posts');
    }

    public function upload(&$file, $base64)
    {
        if ($base64) {

            $contents = file_get_contents($base64);
            $mimeType = Image::make($contents)->mime();

            switch ($mimeType) {
                case 'image/jpeg':
                case 'image/jpg':
                    $extension = '.jpg';
                    break;

                case 'image/png':
                    $extension = '.png';
                    break;

                case 'image/bmp';
                    $extension = '.bmp';
                    break;

                case 'image/gif':
                    $extension = '.gif';
                    break;

                case 'image/tiff':
                case 'image/x-tiff':
                    $extension = '.tiff';
                    break;

                default:
                    throw new \Exception("Invalid Mime-Type file.");
            }

            $imageName = md5(md5(microtime())) . $extension;
            $path = public_path('storage/'. $imageName);
            // dd($imageName);
            
            try {

                Image::make($contents)->save($path);

                //  dd($path);
                
                $file->name = $imageName;
                $file->filename = 'storage/' . $imageName;
                $file->path = 'storage/';
                $file->save();

                return true;
            } catch (Exception $e) {
                dd($e);
            }

            return  false;
        }
    }

    public function uploadFile(&$file, $base64)
    {
        if ($base64) {
            // dd($base64);
            // $contents = file_get_contents($base64['file']);
            $imageName = md5(md5(microtime())) . '.pdf';

            $path = $base64['file']->storeAs('public/pdf', $imageName);

            try {
                
                $file->name = $base64['title'];
                $file->filename = 'storage/pdf/' . $imageName;
                $file->path = 'storage/pdf/';
                $file->save();

                return true;
            } catch (Exception $e) {
                dd($e);
            }

            return  false;
        }
    }
}
