<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepresentativeRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required','min:8'],
            'email' => ['required', 'unique:representatives,email,', 'email:rfc,dns'],
            'cpf' => ['required', 'min:14','max:14', 'unique:representatives,cpf'],
            'phone' => ['required', 'min:15', 'unique:representatives,phone'],
            'city' => ['required'],
            'razao' => ['required'],
            'contact' => ['required','min:15'],
            'cnpj' => ['required', 'min:18','max:18'],
            'n_core_pj' => ['required'],
            'cidade' => ['required'],
            'file' => request()->method() == 'put' ? ['required','mimetypes:application/pdf','max:2000'] : ['required','mimetypes:application/pdf','max:2000'],
            'password' => ['string', 'min:8', 'confirmed'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O campo "Nome Completo" é obrigatório.',
            'name.min' => 'Nome inválido.',
            'phone.required' => 'O campo "Telefone" é obrigatório.',
            'phone.unique' => 'Você já entrou em contato.',
            'phone.min' => 'Telefone inválido',
            'contact.required' => 'O campo "Telefone" é obrigatório.',
            'contact.min' => 'Telefone inválido',
            'n_core_pj.required' => 'O campo "N° Core PJ" é obrigatório.',
            'cpf.required' => 'O campo "CPF" é obrigatório.',
            'cpf.unique' => 'Você já estrou em contato',
            'cpf.min' => '"CPF" inválido',
            'cpf.max' => '"CPF" inválido',

            'cnpj.required' => 'O campo "CNPJ" é obrigatório.',
            'cnpj.min' => 'CNPJ inválido',
            'cnpj.max' => 'CNPJ inválido',
            
            'city.required' => 'O campo "Cidade" é obrigatório.',
            'cidade.required' => 'O campo "Cidade" é obrigatório.',
            'email.required' => 'O campo "E-mail" é obrigatório.',
            'email.unique' => 'Você ja entrou em contato.',
            'email.email' => 'E-mail inválido.',
            'razao.required' => 'O campo "Razão Social" é obrigatório.',
            'file.required' => 'O campo "Anexar Currículo" é obrigatório.',
            'file.mimetypes' => 'Selecione arquivo do tipo PDF.',
            'file.max' => 'Aquivo muito grande. Por favor selecione arquivo de até 2mbs.',

            'password.string' => 'Formato de senha incorreta.',
            'password.min' => 'A senha tem que ter no mínimo 8 carecteres.',
            'password.confirmed' => 'Senha incorreta.'
 
        ];
    }
}
