<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactsRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => ['required','min:3'],
            'phone' => 'required',
            'email' => ['required','email:rfc,dns'],
            'message' => ['required','min:5'],
            'subject' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'O campo "Nome" é obrigatório.',
            'subject.required'  => 'O campo "Assunto" é obrigatório.',
            'name.min'       => 'Nome inválido.',
            'phone.required' => 'O campo "Telefone" é obrigatório.',
            'email.required' => 'O campo "E-mail" é obrigatório.',
            'email.email'    => 'E-mail Inválido',
            'message.required'  => 'O campo "Mensagem" é obrigatório.',
            'message.min'  => 'O campo "Nome" é obrigatório.',
        ];
    }
}
