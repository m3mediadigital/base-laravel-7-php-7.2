<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Http\Model\Contacts;
use App\Http\Model\Representatives;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        View::share('faleNotRead', Contacts::where('type','=','contacts','and','read','=', 0)->count());
        View::share('news', Contacts::where('type','=','newslleter','and','read','=', 0)->count());
        // View::share('represent', Representatives::where('read','=', 0)->count());
    }
}
