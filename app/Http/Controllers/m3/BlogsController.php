<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Blogs;
use App\Http\Model\Files;

class BlogsController extends Controller
{
    
    public function index()
    {
        return view('m3.blogs.index');
    }

    
    public function create()
    {
        return view('m3.blogs.create');
    }

    
    public function store(Request $request)
    {
        $files = new Files;
        $files->upload($file, $request->image);

        $blogs = new Blogs;
        $blogs->setData($blogs, $request->all());
        $blogs->files_id = $files->id;

        try{
            $blogs->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao cadastrar blog. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.blog.index')->with('success','Blog cadastrado com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        return view('m3.blogs.edit', ['blog' => Blogs::find($id)]);
    }

  
    public function update(Request $request, $id)
    {
        $blog = blog::find($id);
        $blog->setData($blog, $request->all());

        if(!$request->image):

            $files = new Files;
            $files->upload($file, $request->image);
            $blog->files_id = $files_id;

        endif;

        try{
            $blogs->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao atualizar blog. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.blog.index')->with('success','Blog atualizado com sucesso!');
    }

  
    public function destroy($id)
    {
        $blog = blog::find($id);

        try{
            $blog->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao apagar blog. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.blog.index')->with('success','Blog apagado com sucesso!');
    }
}
