<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Slides;
use App\Http\Model\Files;

class SlidesController extends Controller
{
    
    public function index()
    {
        return view('m3.slides.index', ['slides' => Slides::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.slides.create');
    }

    
    public function store(Request $request)
    {
        $files = new Files;
        $files->upload($files,$request->image);

        $slides = new Slides;
        $slides->setData($slides, $request->all());
        $slides->files_id = $files->id;

        try{
            $slides->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao cadastrar slide. Por favor entrar em contao com o suporte!');
        }

        return redirect()->route('m3.slides.index')->with('success','Slide cadatrado com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.slides.edit',['slide' => Slides::find($id)]);
    }

  
    public function update(Request $request, $id)
    {
        $slide = Slides::find($id);
        $slide->setData($slide, $request->all());

        // dd($request->all());

        if($request->image){
            $files = new Files;
            $files->upload($files,$request->image);
            $slide->files_id = $files->id;
        }

        try{
            $slide->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao atualizar slide. Por favor entrar em contao com o suporte!');
        }

        return redirect()->route('m3.slides.index')->with('success','Slide atualizado com sucesso!');
    }

  
    public function destroy($id)
    {
        $slide = Slides::find($id);

        try{
            $slide->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao atualizar slide. Por favor entrar em contao com o suporte!');
        }

        return redirect()->route('m3.slides.index')->with('success','Slide apagado com sucesso!');
    }
}
