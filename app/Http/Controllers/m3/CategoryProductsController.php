<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\CategoryProducts;
use App\Http\Model\Pages;

class CategoryProductsController extends Controller
{
    
    public function index()
    {
        return view('m3.products.category',['categorys' => CategoryProducts::paginate(15), 'pages' => Pages::orderBy('title', 'asc')->get()]);
    }

 
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        $category = new CategoryProducts;
        $category->setData($category, $request->all());

        try{
            $category->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao cadastrar categoria. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.produtos.categoria.index')->with('success', 'Categoria cadastrado com sucesso!');
    }

  
    public function show($id)
    {
        $category = CategoryProducts::where('pages_id', $id)->get();
        return response()->json($category);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        $category = CategoryProducts::find($id);
        try{
            $category->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao apagar categoria. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.produtos.categoria.index')->with('success', 'Categoria apagado com sucesso!');
    }
}
