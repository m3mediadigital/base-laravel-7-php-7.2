<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Model\Representatives;

class RepresentativesController extends Controller
{
    
    public function index()
    {
        return view('m3.representatives.index',['representatives' => Representatives::orderBy('id','desc')->paginate(15)]);
    }

    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        $representative = Representatives::find($id);
        $representative->read = 1;
        $representative->save();

        return view('m3.representatives.show',['representative' => $representative]);
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        $representative = Representatives::find($id);
        $representative->setData($representative, $request->all());
        $representative->password = Hash::make($request->password);

        try {
            $representative->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('error','Erro ao ativar o acesso do usuário');
        }

        return redirect()->route('m3.usuarios.index')->with('success','Usuário ativo com sucesso!');
    }

    
    public function destroy($id)
    {
        $r = Representatives::find($id);

        try{
            $r->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao excluir dados. Por favor entre em contato com o suporte!');
        }

        return redirect()->back()->with('success', 'Dados Excluido com sucesso!.');
    }
}
