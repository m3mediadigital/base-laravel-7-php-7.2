<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\CategoryPages;

class CategoryPagesController extends Controller
{
    
    public function index()
    {
        return view('m3.pages.category.index', ['categorys'=> CategoryPages::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.pages.category.create');
    }

    
    public function store(Request $request)
    {
        $category = new CategoryPages();
        $category->setData($category,$request->all());

        try{
           $category->save(); 
        }catch(\Exception $e){
            return redirect()->back()->with('error','Error ao cadastrar categoria. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.paginas.categorias.index')->with('success', 'Categoria cadastrado com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        return view('m3.pages.category.edit', ['category' => CategoryPages::find($id)]);
    }

  
    public function update(Request $request, $id)
    {
        $category = CategoryPages::find($id);
        $category->setData($category,$request->all());

        try{
           $category->save(); 
        }catch(\Exception $e){
            return redirect()->back()->with('error','Error ao cadastrar categoria. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.paginas.categorias.index')->with('success', 'Categoria atualizado com sucesso!');
    }

    
    public function destroy($id)
    {
        // $category = CategoryPages::find($id);

        // try{
        //    $category->delete();
        // }catch(\Exception $e){
            // return redirect()->back()->with('error','Entre em contato com o suporte para solicitar a exclusão!');
        // }

        return redirect()->route('m3.categoria-de-paginas.index')->with('Error', 'Entre em contato com o suporte para solicitar a exclusão!');

    }
}
