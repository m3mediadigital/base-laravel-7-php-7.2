<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Model\Settings;

class SettingsController extends Controller
{
    
    public function index()
    {
        return view('m3.settings.index',['settings' => Settings::paginate(15)]);
    }

   
    public function create()
    {
        return view('m3.settings.create');
    }

    
    public function store(Request $request)
    {
        $setting = new Settings;
        $setting->setData($setting, $request->all());

        try{
            $setting->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao cadastrar configuração. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.configuracoes.index')->with('success','Configuração cadastrada com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.settings.edit', ['setting' => Settings::find($id)]);
    }

    
    public function update(Request $request, $id)
    {
        $setting = Settings::find($id);
        $setting->setData($setting, $request->all());

        try{
            $setting->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao atualizar configuração. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.configuracoes.index')->with('success','Configuração atualizado com sucesso!');
    }

  
    public function destroy($id)
    {
        //
    }
}
