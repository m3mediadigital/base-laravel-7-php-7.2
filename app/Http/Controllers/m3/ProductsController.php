<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Products;
use App\Http\Model\Files;
use App\Http\Model\Pages;
use App\Http\Model\CategoryPages;

class ProductsController extends Controller
{
    
    public function index()
    {
        $pages = Pages::orderBy('title','asc');
        $category = CategoryPages::orderBy('title','asc');
        $products = Products::orderBy('title','asc')->paginate(15);

        if(!empty(request()->title)){
            $produts->where(function($q) {
                $q->orWhere('title', 'like', '%' . request()->name . '%');
            });
        }

        if(!empty(request()->pages_id)){
            $pages->where(function($q) {
                $q->orWhere('id', request()->pages_id);
            });

            $pages = $pages->first();

            $category->where(function($q){
                $q->orWhere('id', $pages->category_pages_id);
            });
            $category = response()->json($category);
        }

        return view('m3.products.index',compact('pages', 'category','products'));
    }

    
    public function create()
    {
        return view('m3.products.create',['pages' => Pages::orderBy('title','asc')->get()]);
    }

    
    public function store(Request $request)
    {
        $files = new Files;
        $files->upload($files, $request->image);

        $products = new Products;
        $products->setData($products, $request->all());
        $products->files_id = $files->id;

        try{
            $files->products()->save($products);

        }catch(\Exception $e){
            return redirect()->with('error','Error ao cadastrar produto. Por favor entrar em contato com suporte');

        }

        return redirect()->route('m3.produtos.lista.index')->with('success','Produto cadastrado com sucesso!');
    }

   
    public function show($id)
    {
        return view('m3.products.content.index', ['product' => Products::find($id)]);
    }

  
    public function edit($id)
    {
        return view('m3.products.edit', ['product' => Products::find($id), 'pages' => Pages::orderBy('title','asc')->get()]);
    }

    
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $product = Products::find($id);
        $product->setData($product, $request->all());

        if($request->image):

            $file = new Files;
            $file->upload($file, $request->image);
            $product->files_id = $file->id;

        endif;

        try{
            $product->save();

        }catch(\Exception $e){
            return redirect()->with('error','Error ao atualizar produto. Por favor entrar em contato com suporte');

        }

        return redirect()->route('m3.produtos.lista.index')->with('success','Produto atualizado com sucesso!');
    }

    
    public function destroy($id)
    {
        $product = Products::find($id);

        try{
            $product->delete();

        }catch(\Exception $e){
            return redirect()->with('error','Error ao apagar produto. Por favor entrar em contato com suporte');
        }

        return redirect()->route('m3.produtos.lista.index')->with('success','Produto apagado com sucesso!');
    }
}
