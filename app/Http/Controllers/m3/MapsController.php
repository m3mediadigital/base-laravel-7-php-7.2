<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Http\Model\Maps;
use Illuminate\Http\Request;

class MapsController extends Controller
{
    
    public function index()
    {
        return view('m3.maps.index', ['maps' => Maps::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.maps.create');
    }

  
    public function store(Request $request)
    {
        $maps = new Maps;
        try{
             $maps->create($request->all());
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao cadastrar Mapa. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.onde-estamos.index')->with('success','Mapa cadastrado com sucesso!');
    }

    
    public function show(Maps $maps)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.maps.edit',['map' => Maps::find($id)]);
    }

    
    public function update(Request $request, $id)
    {
        $map = Maps::find($id);

        try{
            $map->update($request->all());

        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao atualizar Mapa. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.onde-estamos.index')->with('success','Mapa atualizado com sucesso!');
    }

    
    public function destroy($id)
    {
        $map = Maps::find($id);
        try{
            $map->delete($id);
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao apagar Mapa. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.onde-estamos.index')->with('success','Mapa apagado com sucesso!');
    }
}
