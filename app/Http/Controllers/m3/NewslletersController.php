<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Contacts;

class NewslletersController extends Controller
{
    
    public function index()
    {
        return view('m3.contacts.newslleter',['contacts' => Contacts::where('type','newslleter')->paginate(15)]);
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        $contact = Contacts::find($id);

        try{
            $contact->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Erro ao apagar newslleter. Por favor entre em contato com o suporte.');
        }

        return redirect()->back()->with('succcess','Newslleter apagado com sucesso.');
    }
}
