<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\ContentProducts;
use App\Http\Model\Products;

class ContentProductsController extends Controller
{
  
    public function index()
    {
        return view('m3.products.content.index',['content' => ContentProducts::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.products.content.create',['product' => Products::find(request()->id)]);
    }

   
    public function store(Request $request)
    {
        $contents = new ContentProducts;
        $contents->setData($contents, $request->all());

        try{
            $contents->save();
        }catch(\Exception $e){
            return redirect()->with('error','Error ao cadastrar conteúdo. Por favor entrar em contato com suporte');
        }

        return redirect()->route('m3.produtos.lista.show', $request->products_id)->with('success','Conteúdo de Produto cadastrado com sucesso!');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.products.content.edit',['content' => ContentProducts::find($id)]);
    }

    
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $content = ContentProducts::find($id);
        $content->setData($content, $request->all());

        try{
            $content->save();
        }catch(\Exception $e){
            return redirect()->with('error','Error ao atualizar conteúdo. Por favor entrar em contato com suporte');
        }

        return redirect()->route('m3.produtos.lista.show', $request->products_id)->with('success','Conteúdo de Produto atualizado com sucesso!');
    }

  
    public function destroy($id)
    {
        $content = ContentProducts::find($id);

        try{
            $content->delete();
        }catch(\Exception $e){
            return redirect()->with('error','Error ao apagar conteúdo. Por favor entrar em contato com suporte');
        }

        return redirect()->back()->with('success','Conteúdo de Produto apagado com sucesso!');
    }
}
