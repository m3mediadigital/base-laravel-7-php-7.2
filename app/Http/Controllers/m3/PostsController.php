<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Posts;
use App\Http\Model\Pages;
use App\Http\Model\Files;
use App\Http\Model\Gallerys;

class PostsController extends Controller
{
    
    public function index()
    {
        return view('m3.posts.index', ['posts' => Posts::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.posts.create',['pages' => Pages::orderBy('title','asc')->get()]);
    }

   
    public function store(Request $request)
    {
        // dd($request->all());
        $file = new Files;
        $file->upload($file, $request->image);

        $gallerys = new Gallerys;
        $gallerys->save();

        $posts = new Posts;
        $posts->setData($posts, $request->all());
        $posts->gallerys_id = $gallerys->id;
        $posts->files_id = $file->id;

        try{

            $file->posts()->save($posts);
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao cadastrar Postagem. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.postagens.index')->with('success', 'Postagem cadastrado com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.posts.edit',['post' => Posts::find($id), 'pages' => Pages::orderBy('title','asc')->get()]);
    }

    
    public function update(Request $request, $id)
    {        
        $post = Posts::find($id);
        $post->setData($post, $request->all());

        if($post->gallerys_id == null){
            $gallerys = new Gallerys;
            $gallerys->save();

            $post->gallerys_id = $gallerys->id;
        }

        if($request->image){
            $file = new Files;
            $file->upload($file, $request->image);
            $post->files_id = $file->id;
        }

        try{

            $post->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao atualizar Postagem. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.postagens.index')->with('success', 'Postagem atualizado com sucesso!');
    }

   
    public function destroy($id)
    {
        $post = Posts::find($id);

        try{

            $post->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao apagar Postagem. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.postagens.index')->with('success', 'Postagem apagado com sucesso!');
    }
}
