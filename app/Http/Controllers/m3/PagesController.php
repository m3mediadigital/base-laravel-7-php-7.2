<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Pages;
use App\Http\Model\Files;
use App\Http\Model\CategoryPages;

class PagesController extends Controller
{
  
    public function index()
    {
        return view('m3.pages.index',['pages' => Pages::paginate(15)]);
    }

    
    public function create()
    {
        return view('m3.pages.create',['categorys' => CategoryPages::orderBy('title','asc')->get()]);
    }

    
    public function store(Request $request)
    {
        $files = new Files;
        $files->upload($files, $request->image);

        $pages = new Pages;
        $pages->setData($pages, $request->all());
        $pages->files_id = $files->id;

        try{
            $pages->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Error ao cadastrar Página. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.paginas.lista.index')->with('success','Página cadastrada com sucesso!');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        return view('m3.pages.edit', ['page' => Pages::find($id), 'categorys' => CategoryPages::orderBy('title','asc')->get()]);
    }

   
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $page = Pages::find($id);
        $page->setData($page, $request->all());
        
        if(isset($request->image)):
            $files = new Files;
            $files->upload($files, $request->image);
            $page->files_id = $files->id;

        endif;

        try{
            // dd($files);
            $page->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Error ao atualizar Página. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.paginas.lista.index')->with('success','Página atualizada com sucesso!');
    }

    
    public function destroy($id)
    {
        $pages = Pages::find($id);
        if($pages->products->count() >= 1)
            return redirect()->back()->with('error','Error ao apagar Página. Existem Produtos Cadastrados para essa página!');

        try{
            $pages->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error','Error ao apagar Página. Por favor entrar em contato com o suporte!');
        }

        return redirect()->route('m3.paginas.lista.index')->with('success','Página apagado com sucesso!');
    }
}
