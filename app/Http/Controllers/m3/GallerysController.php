<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Posts;
use App\Http\Model\Files;
use App\Http\Model\Gallerys;

class GallerysController extends Controller
{
    
    public function index($id)
    {
        
    }

   
    public function create()
    {
        return view('m3.gallerys.create',['post' => Posts::find(request()->id)]);
    }

    
    public function store(Request $request)
    {
        $post = Posts::find($request->post);
        $file = new Files;

        if($post->gallerys_id == null){
            $gallery = new Gallerys;
            $gallery->save();
            $request['gallery'] = $gallery->id;

            $file->gallerys_id = $gallery->id;
            $post->gallerys_id = $gallery->id;
        }else{
            $file->gallerys_id = $post->gallerys_id;
        }
        
        $file->uploadFile($file, $request->all());

        try{
            $post->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao cadastrar Arquivos. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.galeria.show', $post->id)->with('success', 'Arquivo cadastrado com sucesso!');

    }

    
    public function show($id)
    {
        $post = Posts::find($id);
        $files = Files::where('gallerys_id', $post->gallerys_id)->get();
        return view('m3.gallerys.index', compact('post', 'files'));
    }

    
    public function edit($id)
    {
        $file = Files::find($id);
        $post = Posts::where('gallerys_id', $file->gallery_id)->first();    

        return view('m3.gallerys.edit', compact('file', 'post'));
    }

  
    public function update(Request $request, $id)
    {
        $file = Files::find($id);
        $post = Posts::where('gallerys_id', $file->gallerys_id)->first();

        // dd($request->all());

        $file->name = $request->title;

        if($request->file){

            $gallery = new Gallerys;
            $gallery->save();

            $file = new Files;
            $file->gallerys_id = $gallery->id;
            $file->uploadFile($file, $request->all());
            
            $post->gallerys_id = $gallery->id;
        }else{
            $file->save();
        }
        
        try{
            $post->save();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao atualizar Arquivos. Por favor entre em contato com o suporte!');
        }

        return redirect()->route('m3.galeria.show',$post->id)->with('success', 'Arquivo atualizado com sucesso!');
    }

    
    public function destroy($id)
    {
        $file = Files::find($id);

        try{
            $file->delete();
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Erro ao apagar Arquivos. Por favor entre em contato com o suporte!');
        }

        return redirect()->back()->with('success', 'Arquivo apagado com sucesso!');
    }
}
