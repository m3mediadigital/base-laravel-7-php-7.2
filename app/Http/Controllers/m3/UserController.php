<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

use App\Http\Model\Representatives;

class UserController extends Controller
{
    
    public function index(User $model)
    {
        return view('m3.profile.index', ['users' => Representatives::all()]);
    }

    public function edit($id)
    {
        return view('m3.profile.ativation',['representative' => Representatives::find($id)]);
    }
}
