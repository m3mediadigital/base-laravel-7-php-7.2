<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Model\Contacts;

class ContactsController extends Controller
{
    
    public function index()
    {
        return view('m3.contacts.index',['contacts' => Contacts::paginate(15)]);
    }

    
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        $contact = Contacts::find($id);
        $contact->read = 1;
        $contact->save();

        return view('m3.contacts.show', compact('contact'));
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        $contact = Contacts::find($id);
        $contact->delete();

        return redirect()->route('m3.contatos.index')->with('success','Contato apagado com sucesso!');
    }
}
