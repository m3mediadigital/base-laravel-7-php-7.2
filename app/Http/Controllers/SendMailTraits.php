<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactsRequest;
use App\Http\Requests\RepresentativeRequest;
use App\Mail\Contacts as SendContacts;
use App\Mail\Newslleter;
use App\Mail\SendRepresentatives;
use App\Http\Model\Contacts;
use App\Http\Model\Settings;
use App\Http\Model\Representatives;
use App\Http\Model\Files;


/**
 * @Author Felipe Mendonça
 */
trait SendMailTraits
{
    public function SendContact(ContactsRequest $request)
    {
        $setting = Settings::where('slug','e-mail-principal')->first();
        $email = preg_replace('/\s+/', '', $setting->content);

        $contacts = new Contacts;
        $contacts->setData($contacts, $request);

        $mail = new SendContacts($contacts);
        $mail->subject('Contato astravés do Formulário de Contato!');

        try{
            $contacts->save();

            try{
                Mail::to($email)->send($mail);
            }catch(Exception $e){
                return redirect()->back()->with('error', 'Erro ao enviar contato. Por favor tente novamente!');
            }
            
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Erro ao salvar contato. Por favor tente novamente!');
        }    

        return redirect()->route('contacts')->with('success', 'Contato salvo com sucesso!');
    }

    public function newslleter(Request $request)
    {
        // dd($request->all());
        $setting = Settings::where('slug','e-mail-newslleter')->first();
        $email = preg_replace('/\s+/', '', $setting->content);

        $contacts = new Contacts;
        $contacts->type = 'newslleter';
        $contacts->email = $request->email;
        $contacts->read = 0;

        $mail = new Newslleter($contacts);
        $mail->subject('Contato astravés do Formulário de Newslleter!');

        try{
            $contacts->save();

            try{
                Mail::to($email)->send($mail);
            }catch(Exception $e){
                return redirect()->back()->with('error', 'Erro ao enviar contato. Por favor tente novamente!');
            }
            
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Erro ao salvar contato. Por favor tente novamente!');
        }    

        return redirect()->back()->with('success', 'Contato salvo com sucesso!');
    }

    public function representativePost(RepresentativeRequest $request)
    {
        $setting = Settings::where('slug','e-mail-representante')->first();
        $email = preg_replace('/\s+/', '', $setting->content);

        $file = new Files;
        $request['title'] = 'Representante';
        $file->uploadFile($file, $request->all());

        $representative = new Representatives;
        $request['companies'] = $request->companie_one.', '.$request->companie_two.', '.$request->companie_three;
        $representative->setData($representative, $request->all());
        $representative->files_id = $file->id;

        $mail = new SendRepresentatives($representative);
        $mail->subject('Contato astravés do Formulário de Representantes!');

        try{
            $representative->save();

            try{
                Mail::to($email)->send($mail);
            }catch(Exception $e){
                return redirect()->route('representative')->with('error', 'Erro ao enviar contato. Por favor tente novamente!');
            }
            
        }catch(Exception $e){
            return redirect()->route('representative')->with('error', 'Erro ao salvar contato. Por favor tente novamente!');
        }    

        return redirect()->route('representative')->with('success', 'Contato salvo com sucesso!');
    }
}
