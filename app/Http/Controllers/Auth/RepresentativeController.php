<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;

class RepresentativeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::REPRESENTATIVE;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:representatives')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('representatives');
    }

    public function index()
    {
        return redirect('/area-do-representante');
    }

    public function login(Request $request)
    {
        // $this->validate($request, [
        //     'cpf' => 'required|min:14|max:14',
        //     'password' => 'required'
        // ]);
    

        try {
            
            if (Auth::guard('representatives')->attempt(['cpf' => $request->input('cpf'), 'password' => $request->input('password')]))
                return redirect()->intended(route('pages', ['slug' => 'area-do-representante']));  
            
        } catch (\Exception $e) {
            dd($e);
        }
        

        // if($auth == true){
        //     return redirect()->back(); 
        // }else{
        //     return redirect()->back()->with("error",'Error, por favor entre em contato com o suporte!');
        // }
 
    }
}
