<?php

namespace App\Http\Controllers;

use App\Http\Model\Contacts;
use App\Http\Model\Representatives;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        View::share('faleNotRead', Contacts::where('type','=','contacts','and','read','=', 0)->count());
        View::share('news', Contacts::where('type','=','newslleter','and','read','=', 0)->count());
        // View::share('represent', Representatives::where('read','=', 0)->count());
    }

    public function index()
    {
        return view('m3.dashboard');
    }
}
