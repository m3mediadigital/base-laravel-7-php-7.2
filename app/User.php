<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function pages()
    // {
    //     return $this->belongsTo('App\Http\Model\Pages');
    // }

    // public function categoryPages()
    // {
    //     return $this->belongsTo('App\Http\Model\CateogryPages');
    // }

    // public function products()
    // {
    //     return $this->belongsTo('App\Http\Model\Products');
    // }
}
