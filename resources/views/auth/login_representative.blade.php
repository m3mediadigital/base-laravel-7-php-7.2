<section class="pages">
    <div class="container-fluid">
        <div class="col-12 padding-fluid pt-0">
            <div class="pages-content pb-5"> 
                <img src="//localhost:3000/images/min/barra.png" class="img-fluid blogs-img-bottom w-100" alt="Sustennutri">
                <h1 class="text-uppercase text-cyan position-relative pb-5">
                    <span class="text-teal">Área do</span> Representante
                </h1>
            </div>
        </div>
        <div class="pt-3 padding-fluid">
            <div class="pages-content pl-5">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <p class="text-uppercase text-cyan position-relative" style="font-size: 30px">
                            <span class="text-teal" style="font-size: 30px">Área</span> Representante
                        </p>
                        <p class="pt-0 pb-0 pl-4 padding-fluid">
                            A área do Representante é restrita e exclusiva para representantes e parceiros da Sustennutri. 
                            Faça o login caso já possua acesso ou solicite um no botão ao lado.
                        </p>
                        <div class="contacts">
                            <form action="{{ route('representativeLogin') }}" method="post">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-8">
                                        <input type="tel" class="form-control cpf @error('cpf') is-invalid @enderror " placeholder="  " name="cpf" value="{{ old('cpf') }}">
                                        <label for="cpf">CPF</label>
                                        @error('cpf')<span class="form-text invalid-feedback d-block position-absolute" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                    <div class="form-group col-12 col-md-4">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror " placeholder="  " name="password" value="{{ old('password') }}">
                                        <label for="password">Senha</label>
                                        @error('password')<span class="form-text invalid-feedback d-block position-absolute" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <button type="submit" class="btn btn-teal text-uppercase no-collapse">acessar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                        <a href="{{ route('contacts') }}" class="btn btn-default text-uppercase">solicitar acesso</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>