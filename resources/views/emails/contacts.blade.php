@component('mail::message')
# Novo Contato

Clique no botão para ver as mensagens

@component('mail::button', ['url' => route('m3.contatos.index')])
 Pagina de contato
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent