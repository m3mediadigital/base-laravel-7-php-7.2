@extends('m3.layouts.app', ['title' => __('Representantes')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Representantes'),
        'description' => __('Contatos recebidos através do formulário de Representantes.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Representantes</h3>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Currículo</th>
                                    <th scope="col">Telefone</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Data de recebimento</th>
                                    <th scope="col" style="text-align: right;">Opções</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($representatives as $item)
                                    <tr style="{{ $item->read == 0 ? 'font-weight: bolder' : '' }}"">
                                        <td>{{ $item->name }}</td>
                                        <td><a href="{{ asset($item->files->filename) }}" target="_blank" rel="noopener noreferrer">Clique aqui</a></td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->email}}</td>
                                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
        
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route('m3.representantes.show', $item->id ) }}">
                                                        <i class="fas fa-edit"></i>visualizar
                                                    </a>
                                                    {{ deleteButton( route('m3.representantes.destroy', $item->id)) }} 
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $representatives->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection