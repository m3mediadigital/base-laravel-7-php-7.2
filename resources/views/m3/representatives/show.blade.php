@extends('m3.layouts.app', ['title' => __('Representantes')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Representantes'),
        'description' => __('Contatos recebidos através do formulário de representantes.'),
        'class' => 'col-lg-12'
    ])   

        <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Representante</h3>
                            </div>
                            <div class="col-4 text-right">
                                <button type="button" onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="btn btn-sm btn-primary">Apagar</button>
                            </div>
                            <form id="delete-form" action="{{ route('m3.representantes.destroy', $representative->id) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                    <p>{{ $representative->name }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Telefone') }}</label>
                                    <p>{{ $representative->phone }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('E-mail') }}</label>
                                    <p>{{ $representative->email }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('CPF') }}</label>
                                    <p>{{ $representative->cpf }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Cidade') }}</label>
                                    <p>{{ $representative->city }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Currículo') }}</label>
                                    <p><a href="{{ asset($representative->files->filename) }}" target="_blank" rel="noopener noreferrer">Clique aqui</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Empresas') }}</label>
                                    <p>{{ $representative->companies }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-6">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Razão Social') }}</label>                                
                                    <p>{{ $representative->social }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Contato') }}</label>                                
                                    <p>{{ $representative->contact }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('CNPJ') }}</label>                                
                                    <p>{{ $representative->cnpj }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('N° Core PJ') }}</label>                                
                                    <p>{{ $representative->n_core_pj }}</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Cidade') }}</label>                                
                                    <p>{{ $representative->cidade }}</p>
                                </div>
                            </div>
                        </div>                
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>        
        @include('m3.layouts.footers.auth')
    </div>
@endsection