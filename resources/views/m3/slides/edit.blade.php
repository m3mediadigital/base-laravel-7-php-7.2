@extends('m3.layouts.app', ['title' => __('Editar de Slides')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Editar Slide'),
        'description' => __(''),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Editar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.slides.update', $slide->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-12 col-xl-6">
                                    <div class="form-row">
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('link') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Link</label>
                                                <input type="text" class="form-control  @error('link') has-danger @enderror" name="link" value="{{ $slide->link }}">
                                                @error('link')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('active') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Ativo<em>*</em></label>
                                                <select name="active" id="pages" class="form-control @error('active') has-danger @enderror">
                                                    <option value="1" {{ $slide->active == '1' ? 'selected' : '' }}>Sim</option>
                                                    <option value="0" {{ $slide->active == '0' ? 'selected' : '' }}>Não</option>
                                                </select>
                                                
                                                @error('active')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('position') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Posição<em>*</em></label>
                                                <input type="number" class="form-control  @error('position') has-danger @enderror" name="position" value="{{ $slide->position }}">                                  
                                                @error('position')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 input-daterange datepicker row align-items-center">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">Início<em>*</em></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="Start date" name="start_at" type="text" value="{{ date('d-m-Y', strtotime($slide->start_at)) ? date('d-m-Y', strtotime($slide->start_at)) : now()->format('d/m/Y') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">Finalização<em>*</em></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="End date" name="finish_at" type="text" value={{ date('d-m-Y', strtotime($slide->finish_at)) ? date('d-m-Y', strtotime($slide->finish_at)) : now()->format('d/m/Y') }}>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-xl-6">
                                    <div class="form-group @error('image') has-danger @enderror">
                                        <label class="form-control-label" for="input-img">{{ __('Imagem') }}<em>*</em></label>
                                        <div class="custom-file">                                    
                                            <input type="file" name="image" id="input-image" class="custom-file-input @error('image') has-danger @enderror" lang="pt-br">
                                            <label class="custom-file-label" for="customFileLang">Selecione uma Imagem</label>
                                        </div>

                                        <img  id="imagem" class="img-fluid" src="{{ asset($slide->files->filename) }}">
                                        @error('image')                                    
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('img') }}</strong>
                                            </span>
                                        @enderror  
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script src="{{ asset('argon/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader()
                reader.onload = function(e) {
                    $('#imagem').attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0])
            }
        }
        $('#input-image').change(function(){
            readURL(this)
        })
    </script>
@endpush