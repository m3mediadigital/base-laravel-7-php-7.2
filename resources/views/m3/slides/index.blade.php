@extends('m3.layouts.app', ['title' => __('Slides')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Slides'),
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Slides</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a class="btn btn-danger btn-sm" href="{{ route('m3.slides.create') }}" >
                                    Adicionar
                                </a>                                
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th scope="col">Link</th>
                                    <th scope="col">Ativo</th>
                                    <th scope="col">Posição</th>
                                    <th scope="col">Início</th>
                                    <th scope="col">Final</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($slides as $item)                                   
                             
                                <tr>
                                    <td><img src="{{ asset($item->files->filename) }}" width="200" height="50" alt="{{ $item->title }}"></td>
                                    <td>{{ $item->link }}</td>
                                    <td>{{ $item->active }}</td>
                                    <td>{{ $item->position}}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->start_at)) }}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->finish_at)) }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.slides.edit', $item) }}"><i class="fas fa-edit"></i>Editar</a>
                                                {{ deleteButton(route('m3.slides.destroy', $item->id)) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $slides  ->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection