@extends('m3.layouts.app', ['title' => __('ativação de Acesso')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Olá') . ' '. auth()->user()->name,
        'description' => __('Nessa seção você autoriza os acessos a area do representante cadastrarndo apenas a senha'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Cadastrar senha de acesso do representante') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('m3.representantes.update', $representative->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-row">
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group @error('name') has-danger @enderror">
                                        <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                        <input type="text" class="form-control  @error('name') has-danger @enderror" name="name" value="{{ $representative->name  }}">
                                        @error('name')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group @error('phone') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Telefone') }}</label>
                                        <input type="tel" class="form-control sp_celphones @error('phone') has-danger @enderror" name="phone" value="{{ $representative->phone  }}">
                                        @error('phone')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group @error('email') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('E-mail') }}</label>
                                        <input type="email" class="form-control @error('email') has-danger @enderror" name="email" value="{{ $representative->email  }}">
                                        @error('email')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group @error('cpf') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('CPF') }}</label>
                                        <input type="tel" class="form-control cpf @error('cpf') has-danger @enderror" name="cpf" value="{{ $representative->cpf  }}">
                                        @error('cpf')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group @error('city') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Cidade') }}</label>
                                        <input type="text" class="form-control city @error('city') has-danger @enderror" name="city" value="{{ $representative->city  }}">
                                        @error('city')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-crm">{{ __('Currículo') }}</label>
                                        <p><a href="{{ asset($representative->files->filename) }}" target="_blank" rel="noopener noreferrer">Clique aqui</a></p>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-crm">{{ __('Empresas') }}</label>
                                        <p>{{ $representative->companies }}</p>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="form-group @error('social') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Razão Social') }}</label>
                                        <input type="text" class="form-control social @error('social') has-danger @enderror" name="social" value="{{ $representative->social  }}">
                                        @error('social')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-3">
                                    <div class="form-group @error('contact') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Contato') }}</label>
                                        <input type="tel" class="form-control sp_celphones @error('contact') has-danger @enderror" name="contact" value="{{ $representative->contact  }}">
                                        @error('text')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror                             
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-3">
                                    <div class="form-group @error('cnpj') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('CNPJ') }}</label>     
                                        <input type="text" class="form-control cnpj @error('cnpj') has-danger @enderror" name="cnpj" value="{{ $representative->cnpj  }}">
                                        @error('cnpj')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-3">
                                    <div class="form-group @error('n_core_pj') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('N° Core PJ') }}</label>      
                                        <input type="text" class="form-control @error('n_core_pj') has-danger @enderror" name="n_core_pj" value="{{ $representative->n_core_pj  }}">
                                        @error('n_core_pj')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-3">
                                    <div class="form-group @error('cidade') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Cidade') }}</label>
                                        <input type="text" class="form-control  @error('cidade') has-danger @enderror" name="cidade" value="{{ $representative->cidade  }}">
                                        @error('cidade')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6">
                                    <div class="form-group @error('password') has-danger @enderror">
                                        <label class="form-control-label" for="input-crm">{{ __('Senha') }}</label>
                                        <input type="password" class="form-control  @error('password') has-danger @enderror" name="password" value="">
                                        @error('password')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Atualizar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00000';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        })
    </script>
@endpush