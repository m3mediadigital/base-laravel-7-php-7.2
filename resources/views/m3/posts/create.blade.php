@extends('m3.layouts.app', ['title' => __('Cadastro de Postagem')])

@push('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" href="{{ asset('argon/css/custom_select.css') }}">
@endpush

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Adicionar Postagem'),
        'description' => __('Selecione uma Página para que a postagem possa ser separado corretamente por páginas.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Cadastrar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.postagens.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-12 col-xl-8">
                                    <div class="form-row">
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('title') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                                <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ old('title') }}">
                                                @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('subtitle') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Subtitulo</label>
                                                <input type="text" class="form-control  @error('subtitle') has-danger @enderror" name="subtitle" value="{{ old('subtitle') }}">
                                                @error('subtitle')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('link') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">YouTube</label>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1"><i class="fab fa-youtube"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control  @error('link') has-danger @enderror" name="link" value="{{ old('link') }}" placeholder="k0k33cyUHBI" aria-label="k0k33cyUHBI" aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                @error('link')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('pages_id') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Página<em>*</em></label>
                                                <div class="input-group input-group-merge">
                                                    <select name="pages_id" id="pages" class="form-control js-example-basic-single @error('pages_id') has-danger @enderror">
                                                        <option value="0">Selecione</option>
                                                        @foreach ($pages as $item)
                                                            <option value="{{ $item->id }}" {{ old('pages_id') == $item->id ? 'selected' : '' }}>{{ $item->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                                @error('pages_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        {{-- <div class="form-group col-12 col-m-12 col-lg-12 @error('type') has-danger @enderror">
                                            <label for="categorys">Categoria<em>*</em></label>
                                            <div class="input-group input-group-merge">
                                                <select name="type" class="form-control  @error('type')is-invalid @enderror" id="">
                                                    <option value="">Selecione</option>
                                                    @foreach ($type as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="text" name="type_product" class="form-control" placeholder="Tipo de produto" id="category" style="display: none">
                                                @error('type') <div class="invalid-feedback d-block"> {!! $message !!} </div>  @enderror
                                           </div>
                                        </div> --}}

                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-xl-4">
                                    <div class="form-group @error('image') has-danger @enderror">
                                        <label class="form-control-label" for="input-img">{{ __('Imagem') }}<em>*</em></label>
                                        <div class="custom-file">                                    
                                            <input type="file" name="image" id="input-image" class="custom-file-input @error('image') has-danger @enderror" lang="pt-br">
                                            <label class="custom-file-label" for="customFileLang">Selecione uma Imagem</label>
                                        </div>

                                        <img  id="imagem" class="img-fluid" src="">
                                        @error('image')                                    
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('img') }}</strong>
                                            </span>
                                        @enderror  
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('content') has-danger @enderror">
                                        <label class="form-control-label" for="input-content_top">{{ __('Conteúdo') }} </label>
                                        <textarea name="content" id="editor" class="form-control @error('content') has-danger @enderror">{{ old('content') }}</textarea>

                                        @error('content') 
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

    <script src="//cdn.ckeditor.com/4.14.1/basic/ckeditor.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader()
                reader.onload = function(e) {
                    $('#imagem').attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0])
            }
        }
        $('#input-image').change(function(){
            readURL(this)
        }) 

        CKEDITOR.replace('editor',{
            width: '100%',
            height: 300
        });
        $(document).ready(function(){
            $('.js-example-basic-single').select2();
        })
        
   
    </script>
@endpush