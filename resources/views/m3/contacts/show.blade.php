@extends('m3.layouts.app', ['title' => __('Contatos')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Contatos'),
        'description' => __('Contatos recebidos através do formulário de contato.'),
        'class' => 'col-lg-12'
    ])   

        <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Contato</h3>
                            </div>
                            <div class="col-4 text-right">
                                <button type="button" onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="btn btn-sm btn-primary">Apagar</button>
                            </div>
                            <form id="delete-form" action="{{ route('m3.contatos.destroy', $contact->id) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-8">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                    <p>{{ $contact->name }}</p>
                                </div>
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Telefone') }}</label>
                                    <p>{{ $contact->phone }}</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('E-mail') }}</label>
                                    <p>{{ $contact->email }}</p>
                                </div>
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Assunto') }}</label>
                                    <p>{{ $contact->subject }}</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group{{ $errors->has('crm') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-crm">{{ __('Mensagem') }}</label>
                                    <p>{{ $contact->message }}</p>
                                </div>
                            </div>
                        </div>
                
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>        
        @include('m3.layouts.footers.auth')
    </div>
@endsection