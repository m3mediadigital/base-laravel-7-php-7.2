@extends('m3.layouts.app', ['title' => __('Newslleter')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Newslleter'),
        'description' => __('Contatos recebidos através do formulário de Newslleter.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Newslleter</h3>
                            </div>
                            {{-- <div class="col-4 text-right">
                                <div class="dropdown dropleft">
                                    <a class="btn btn-danger dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Adicionar
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a href="{{ route('m3.produtos.categoria.index') }}" class="dropdown-item"><i class="far fa-caret-square-right"></i>Categoria de Produto</a>
                                        <a href="{{ route('m3.produtos.lista.create') }}" class="dropdown-item"><i class="fas fa-plus-square"></i>Produto</a>
                                    </div>
                                </div>
                                
                            </div> --}}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Data de envio</th>
                                    <th scope="col" style="text-align: right;">Opções</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contacts as $item)
                                    <tr style="{{ $item->read == 0 ? 'font-weight: bolder' : '' }}"">
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->email}}</td>
                                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    {{ deleteButton( route('m3.contatos.destroy', $item->id)) }} 
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $contacts->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection