@extends('m3.layouts.app', ['title' => __('Cadastro de Produto')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Adicionar produto'),
        'description' => __('Cada produto, tem que ter uma pagina e uma cagoria'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Cadastrar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.produtos.lista.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-12 col-xl-8">
                                    <div class="form-row">
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('title') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                                <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ old('title') }}">
                                                @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('pages_id') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Página<em>*</em></label>
                                                <select name="pages_id" id="pages" class="form-control @error('pages_id') has-danger @enderror">
                                                    <option value="0">Selecione</option>
                                                    @foreach ($pages as $item)
                                                        <option value="{{ $item->id }}" {{ old('pages_id') == $item->id ? 'selected' : '' }}>{{ $item->title }}</option>
                                                    @endforeach
                                                </select>
                                                
                                                @error('pages_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('category_pages_id') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Categoria<em>*</em></label>
                                                <select name="category_pages_id" id="category" class="form-control @error('category_pages_id') has-danger @enderror" disabled>
                                                    <option value="">Selecione uma Página</option>
                                                </select>                                                
                                                @error('category_pages_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('index') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Mostra na index<em>*</em></label>
                                                <select name="index" id="category" class="form-control @error('index') has-danger @enderror">
                                                    <option value="0" {{  old('index') == 0 ? 'selected' : '' }}>Não</option>
                                                    <option value="1" {{  old('index') == 1 ? 'selected' : '' }}>Sim</option>
                                                </select>                                                
                                                @error('index')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-lg-12 col-xl-4">
                                    <div class="form-group @error('image') has-danger @enderror">
                                        <label class="form-control-label" for="input-img">{{ __('Imagem') }}<em>*</em></label>
                                        <div class="custom-file">                                    
                                            <input type="file" name="image" id="input-image" class="custom-file-input @error('image') has-danger @enderror" lang="pt-br">
                                            <label class="custom-file-label" for="customFileLang">Selecione uma Imagem</label>
                                        </div>

                                        <img  id="imagem" class="img-fluid" src="">
                                        @error('image')                                    
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('img') }}</strong>
                                            </span>
                                        @enderror  
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('content') has-danger @enderror">
                                        <label class="form-control-label" for="input-content_top">{{ __('Descrição') }} <em>*</em></label>
                                        <textarea name="content" id="editor" class="form-control @error('content') has-danger @enderror">{{ old('content') }}</textarea>

                                        @error('content') 
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader()
                reader.onload = function(e) {
                    $('#imagem').attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0])
            }
        }
        $('#input-image').change(function(){
            readURL(this)
        }) 

        CKEDITOR.replace('editor',{
            width: '100%',
            height: 300
        });
   

        $(document).ready(function(){
            $('#pages').change(function(){
                let id = $('#pages option:selected').val()
                
                if(id == 0){
                    $('#category').attr('disabled', true)
                }
                if(id > 0){
                    $.ajax({
                        type: 'GET',
                        url: '/m3/produtos/categoria/' + id,
                        dataType:'json',
                        cache:false,
                        success : function(data){

                           $('#category').attr('disabled', false)
                        
                            let options = '<options value="">Selecione</option>'
                            data.map((item) => {
                                options += '<option value="' + item.id +'">' + item.title + '</option>'
                            })
                        
                            $('#category').html(options).show()
                            
                        }, error: function(){ 
                                $('#category').attr('disabled', true)
                                $.notify({ icon: 'fas fa-bomb', title: "<strong>Ops ...!</srtong>", message: 'Por favor recarregue a página e tente novamente',},{type: 'danger' }); 
                           }
                    })
                }
            })
        })
    </script>
@endpush