@extends('m3.layouts.app', ['title' => __('Produtos')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Produtos'),
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Páginas</h3>
                            </div>
                            <div class="col-4 text-right">
                                <div class="dropdown dropleft">
                                    <a class="btn btn-danger dropdown-toggle btn-sm" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Adicionar
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a href="{{ route('m3.produtos.categoria.index') }}" class="dropdown-item"><i class="far fa-caret-square-right"></i>Categoria de Produto</a>
                                        <a href="{{ route('m3.produtos.lista.create') }}" class="dropdown-item"><i class="fas fa-plus-square"></i>Produto</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Categoria</th>
                                    <th scope="col">Página</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $item)                                   
                             
                                <tr>
                                    <td><img src="{{ Storage::url($item->files->name) }}" width="100" height="50" alt="{{ $item->title }}"></td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->category_products->title }}</td>
                                    <td>{{ $item->pages->title }}</td>
                                    <td>{{ $item->slug }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.produtos.lista.edit', $item) }}"><i class="fas fa-edit"></i>Editar</a>
                                                <a class="dropdown-item" href="{{ route('m3.produtos.lista.show', $item) }}"><i class="fas fa-file-contract"></i>Conteúdo</a>
                                                {{ deleteButton(route('m3.produtos.lista.destroy', $item->id)) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $products->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection