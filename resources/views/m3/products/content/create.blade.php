@extends('m3.layouts.app', ['title' => __('Conteúdo do Produto')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Adcionar conteúdo:'. $product->title),
        'description' => __('Cada produto, tem que ter uma pagina e uma cagoria'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Cadastrar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.produtos.conteudo.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="products_id" value="{{ $product->id }}">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('title') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                        <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>                                        
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('content') has-danger @enderror">
                                        <label class="form-control-label" for="input-content_top">{{ __('Conteúdo') }} <em>*</em></label>
                                        <textarea name="content" id="editor" class="form-control @error('content') has-danger @enderror">{{ old('content') }}</textarea>

                                        @error('content') 
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>

        CKEDITOR.replace('editor',{
            width: '100%',
            height: 300
        });
   
    </script>
@endpush