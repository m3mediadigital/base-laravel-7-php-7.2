@extends('m3.layouts.app', ['title' => __('Categoria de Produtos')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Categorias de Produtos'),
        'description' => __('Cadastre a categoria de produtos ligado a uma página brevemente cadastrada.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Categoria de Páginas</h3>
                            </div>
                            <div class="col-4 text-right">
                                <button class="btn btn-success btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Adicionar
                                </button>
                            </div>
                            <div class="col-12">
                                <div class="collapse pt-5" id="collapseExample">
                                    <form class="" method="post" action="{{ route('m3.produtos.categoria.store') }}">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-12 col-md-6 col-lg-5 col-xl-6">
                                                <input type="text" class="form-control" id="inlineFormInputName2" name="title" placeholder="Categoria de produto">
                                            </div>
                                            <div class="form-group col-12 col-md-6 col-lg-5 col-xl-5">
                                                <select name="pages_id" class="form-control">
                                                    <option value="">Selecione</option>
                                                    @foreach ($pages as $item)
                                                        <option value="{{ $item->id }}">{{ $item->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-12 col-md-12 col-lg-2 col-xl-1">
                                                <button type="submit" class="btn btn-success mb-2 w-auto">Cadastrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Paginas</th>
                                    <th scope="col">Produtos</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categorys as $item)                                   
                             
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->pages->title }}</td>
                                    <td>{{ $item->products->count() }}</td>
                                    <td>{{ $item->slug }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.produtos.categoria.destroy', $item->id) }}"><i class="fas fa-edit"></i>Editar</a>
                                                {{ deleteButton(route('m3.produtos.categoria.destroy', $item->id)) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $categorys->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

@endpush