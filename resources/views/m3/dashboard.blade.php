@extends('m3.layouts.app',['title' => 'Seja Bem Vindo'])

@section('content')
          @include('m3.layouts.headers.header', [
        'title' => __('Seja Bem vindo '. auth()->user()->name),
        'description' => __('Nesse Painel adminitrativo, você controla todas as informações presentente em seu site.'),
        'class' => 'col-lg-12'
    ])  
    
    <div class="container-fluid mt--7">
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush