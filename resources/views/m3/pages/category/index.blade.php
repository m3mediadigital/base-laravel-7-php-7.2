@extends('m3.layouts.app', ['title' => __('Categoria de Página')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Categoria de Página'),
        'description' => __('Essa seção cria a separação de conteúdo das página do menu animais e produtos. Após casdatrar a categaria, só aparecerar no site após o cadastro de página com o mesmo'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Categoria de Páginas</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a class="btn btn-success btn-sm" href="{{ route('m3.paginas.categorias.create') }}">
                                    Adicionar
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Ativo</th>
                                    <th scope="col">Posição</th>
                                    <th scope="col">Paginas</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categorys as $item)                                   
                             
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td><p class="btn-sm badge badge-{{ $item->active == 1 ? 'success' : 'danger' }}">{{ $item->active == 1 ? 'SIM' : 'NÃO' }}</p></td>
                                    <td>{{ $item->position }}</td>
                                    <td>{{ $item->pages ? $item->pages->count() : '' }}</td>
                                    <td>{{ $item->slug }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.paginas.categorias.edit', $item->id) }}"><i class="fas fa-edit"></i>Editar</a>
                                                {{-- {{ deleteButton(route('m3.paginas.destroy', $item->id)) }} --}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $categorys->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

@endpush