@extends('m3.layouts.app', ['title' => __('Adicionar Categoria de Página')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => 'Categoria de Página',
        'description' => __('Essa seção cria a separação de conteúdo das página do menu animais e produtos. Após casdatrar a categaria, só aparecerar no site após o cadastro de página com o mesmo'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Adicionar</h3>
                            </div>
                            <div class="col-4 text-right">
                                
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.paginas.categorias.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('title') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                        <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group @error('position') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Posição<em>*</em></label>
                                        <input type="number" class="form-control  @error('position') has-danger @enderror" name="position" value="{{ old('position') }}">
                                        @error('position')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group @error('active') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Ativo</label>
                                        <select name="active" class="form-control @error('active') has-danger @enderror">
                                            <option value="1" {{ old('active') == 1 ? 'selected' : '' }}>Sim</option>
                                            <option value="0" {{ old('active') == 0 ? 'selected' : '' }}>Não</option>
                                        </select>
                                        @error('active')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')
@endpush