@extends('m3.layouts.app', ['title' => __('Editar '. $page->title)])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => $page->title,
        'description' => __('This is your profile page. You can see the progress you\'ve made with your work and manage your projects or assigned tasks'),
        'class' => 'col-lg-7'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Páginas</h3>
                            </div>
                            <div class="col-4 text-right">
                                
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.paginas.lista.update', $page->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-8">
                                    <div class="form-row">
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('title') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                                <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ $page->title }}">
                                                @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12">
                                            <div class="form-group @error('subtitle') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Subtitulo</label>
                                                <input type="text" class="form-control @error('subtitle') has-danger @enderror" name="subtitle" value="{{ $page->subtitle }}">
                                                @error('subtitle')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- <div class="col-12 col-lg-12">
                                            <div class="form-group @error('category_pages_id') has-danger @enderror">
                                                <label for="example-text-input" class="form-control-label">Categoria<em>*</em></label>
                                                <select name="category_pages_id" class="form-control @error('category_pages_id') has-danger @enderror">
                                                    <option value="">Selecione</option>
                                                    @foreach ($categorys as $item)
                                                        <option value="{{ $item->id }}" {{ $page->category_pages_id == $item->id ? 'selected' : '' }}>{{ $item->title }}</option>
                                                    @endforeach
                                                </select>
                                                
                                                @error('category_pages_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group @error('image') has-danger @enderror">
                                        <label class="form-control-label" for="input-img">{{ __('Imagem') }}<em>*</em></label>
                                        <div class="custom-file">                                    
                                            <input type="file" name="image" id="input-image" class="custom-file-input @error('image') has-danger @enderror" lang="pt-br">
                                            <label class="custom-file-label" for="customFileLang">Selecione uma Imagem</label>
                                        </div>

                                        <img  id="imagem" class="img-fluid" src="{{ asset($page->files->filename) }}">
                                        @error('image')                                    
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{!! $message !!}</strong>
                                            </span>
                                        @enderror  
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('content') has-danger @enderror">
                                        <label class="form-control-label" for="input-content_top">{{ __('Conteúdo') }} <em>*</em></label>
                                        <textarea name="content" id="editor" class="form-control @error('content') has-danger @enderror">{{ $page->content }}</textarea>

                                        @error('content') 
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Atualizar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader()
                reader.onload = function(e) {
                    $('#imagem').attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0])
            }
        }
        $('#input-image').change(function(){
            readURL(this)
        }) 

        CKEDITOR.replace('editor',{
            width: '100%',
            height: 300
        });        
    </script>
@endpush