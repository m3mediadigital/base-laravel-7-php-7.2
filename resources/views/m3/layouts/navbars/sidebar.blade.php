<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('m3') }}">
            <img src="{{ asset('argon/img/logo.png') }}" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        {{-- <span class="avatar avatar-sm rounded-circle">
                        {{-- <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg"> 
                        </span> --}}
                        <div class="media-body ml-2 d-lg-none">
                            <span class="mb-0 text-sm  font-weight-bold">{{ auth()->user()->name }}</span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('m3.profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('Minha conta') }}</span>
                    </a>
                    <a href="{{ route('m3.configuracoes.index') }}" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Configurações') }}</span>
                    </a>
                    {{-- <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a> --}}
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Sair') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('m3') }}">
                            <img src="{{ asset('argon/img/favicon.png') }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('m3') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Início') }}
                    </a>
                </li>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Contatos</h6>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('m3.contatos.index') || Route::is('m3.contatos.create') || Route::is('m3.contatos.edit') ? 'text-warning' : '' }}" href="{{ route('m3.contatos.index') }}">
                        <i class="ni ni-like-2 text-warning"></i> {{ __('Contatos') }}
                        @if(@$faleNotRead > 0)
                            <span class="badge badge-primary position-absolute right-2">{{ @$faleNotRead }}</span>
                        @endif
                    </a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link {{ Route::is('m3.newslleters.index') ? 'text-warning' : '' }}" href="{{ route('m3.newslleters.index') }}">
                        <i class="fas fa-newspaper text-warning"></i> {{ __('Newslleter') }}
                        @if(@$news > 0)
                            <span class="badge badge-primary position-absolute right-2">{{ @$news }}</span>
                        @endif
                    </a>
                </li>
            </ul>
             <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Configurações</h6>
            <!-- Navigation -->
             <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.paginas.lista.index') || Route::is('m3.paginas.lista.create') || Route::is('m3.paginas.lista.edit') ? ' active' : '' }}" href="{{ route('m3.paginas.lista.index') }}">
                        <i class="fas fa-file-alt" style="color: rgb(6, 175, 161)"></i>{{ __('Páginas Estáticas') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.produtos.lista.index') || Route::is('m3.produtos.lista.create') || Route::is('m3.produtos.lista.edit') ? ' active' : '' }}" href="{{ route('m3.produtos.lista.index') }}">
                        <i class="fas fa-shopping-cart" style="color: rgb(8, 107, 238)"></i>{{ __('Produtos') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.slides.index') || Route::is('m3.slides.create') || Route::is('m3.slides.edit') ? ' active' : '' }}" href="{{ route('m3.slides.index') }}">
                        <i class="fab fa-slideshare" style="color: rgb(45, 158, 88)"></i>{{ __('Slides') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.postagens.index') || Route::is('m3.postagens.create') || Route::is('m3.postagens.edit') ? ' active' : '' }}" href="{{ route('m3.postagens.index') }}">
                        <i class="fab fa-blogger-b" style="color: rgba(204, 20, 91, 0.795)"></i>{{ __('Postagens') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.configuracoes.index') || Route::is('m3.configuracoes.create') || Route::is('m3.configuracoes.edit') ? ' active' : '' }}" href="{{ route('m3.configuracoes.index') }}">
                        <i class="fas fa-sliders-h" style="color: rgba(115, 20, 240, 0.795)"></i>{{ __('Configurações') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link{{ Route::is('m3.onde-estamos.index') || Route::is('m3.onde-estamos.create') || Route::is('m3.onde-estamos.edit') ? ' active' : '' }}" href="{{ route('m3.onde-estamos.index') }}">
                        <i class="fas fa-route" style="color: orange"></i>{{ __('Onde Estamos') }}
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-examples">
                        <i class="fab fa-laravel" ></i>
                        <span class="nav-link-text">{{ __('Usuários') }}</span>
                    </a>

                    <div class="collapse" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('m3.profile.edit') }}">
                                    {{ __('Minha conta') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('m3.usuarios.index') }}">
                                    {{ __('Usuários') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>