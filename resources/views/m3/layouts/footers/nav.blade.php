<div class="row align-items-center justify-content-end">
    <div class="col-12">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} Desenvolvido por <a href="https://novam3.com.br" class="font-weight-bold ml-1 text-success" target="_blank">Nova M3</a> 
        </div>
    </div>
</div>