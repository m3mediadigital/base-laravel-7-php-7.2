@extends('m3.layouts.app', ['title' => __('Arquivos das Postagens')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __($file->name),
        'description' => __('Envie um novo arqueivo para substituir o arquivo que já existe.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Editar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.galeria.update', $file) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input type="hidden" name="post" value="{{ $post->id }}">
                        <div class="card-body">                            
                            <div class="form-group @error('title') has-danger @enderror">
                                <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ $file->name }}">
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group @error('file') has-danger @enderror">
                                <label class="form-control-label" for="input-img">{{ __('Arquivo') }}<em>*</em></label>
                                <div class="custom-file">                                    
                                    <input type="file" name="file" id="input-file" class="custom-file-input @error('file') has-danger @enderror" lang="pt-br">
                                    <label class="custom-file-label" for="customFileLang">Selecione um arquivo</label>
                                </div>

                                <img  id="filem" class="img-fluid" src="">
                                @error('file')                                    
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror  
                            </div>
                        </div>
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                <button type="submit" class="btn btn-success">Cadastrar</button>
                            </nav>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')
   
@endpush