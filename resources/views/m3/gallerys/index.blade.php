@extends('m3.layouts.app', ['title' => __('Arquivos')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => $post->title,
        'description' => __(''),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Arquivos da Postagem</h3>
                            </div>
                            <div class="col-4 text-right">
                                <div class="dropdown dropleft">
                                    <a class="btn btn-danger btn-sm" href="{{ route('m3.galeria.create', ['id' => $post->id]) }}">
                                        Adicionar
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($files as $item)
                                
                                <tr>
                                    <td><a href="{{ env('APP_URL').'/'.$item->filename }}">{{ $item->name }}</a></td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.galeria.edit', $item) }}"><i class="fas fa-edit"></i>Editar</a>
                                                {{ deleteButton(route('m3.galeria.destroy', $item->id)) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection