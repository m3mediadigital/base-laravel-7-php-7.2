@extends('m3.layouts.app', ['title' => __('Onde Estamos')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Onde Estamos'),
        'description' => __('Cadastrate endereço, contato, e-mail para recebimento de leads, google analitycs, dentre outras coisas.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Configurações</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a class="btn btn-danger dropdown-toggle btn-sm" href="{{ route('m3.onde-estamos.create') }}">
                                    Adicionar
                                </a>                                
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">endereço</th>
                                    <th scope="col">Horario de Funcionamento</th>
                                    <th scope="col">Criado</th>
                                    <th scope="col">Atualizado</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($maps as $item)                                   
                             
                                <tr>                                    
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->hours }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('m3.onde-estamos.edit', $item) }}"><i class="fas fa-edit"></i>Editar</a>
                                                {{ deleteButton(route('m3.onde-estamos.destroy', $item->id)) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $maps->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection