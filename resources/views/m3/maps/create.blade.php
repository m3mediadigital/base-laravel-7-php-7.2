@extends('m3.layouts.app', ['title' => __('Cadastro de Configurações')])

@section('content')
    @include('m3.layouts.headers.header', [
        'title' => __('Adicionar configuração'),
        'description' => __('Cadastrate endereço, contato, e-mail para recebimento de leads, google analitycs, dentre outras coisas.'),
        'class' => 'col-lg-12'
    ])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Cadastrar</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('m3.produtos.create') }}" class="btn btn-sm btn-primary">Adicionar</a> --}}
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('m3.onde-estamos.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('title') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Título<em>*</em></label>
                                        <input type="text" class="form-control  @error('title') has-danger @enderror" name="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('address') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Endereço<em>*</em></label>
                                        <input type="text" class="form-control  @error('address') has-danger @enderror" name="address" value="{{ old('address') }}">
                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('hours') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Horario de Funcionamento<em>*</em></label>
                                        <input type="text" class="form-control  @error('hours') has-danger @enderror" name="hours" value="{{ old('hours') }}">
                                        @error('hours')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('link') has-danger @enderror">
                                        <label for="example-text-input" class="form-control-label">Link para o botao Mobile<em>*</em></label>
                                        <input type="text" class="form-control  @error('link') has-danger @enderror" name="link" value="{{ old('link') }}">
                                        @error('link')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                    <div class="form-group @error('maps') has-danger @enderror">
                                        <label class="form-control-label" for="input-maps_top">{{ __('Mapa') }} <em>*</em></label>
                                        <textarea name="maps" class="form-control @error('maps') has-danger @enderror">{{ old('maps') }}</textarea>

                                        @error('maps') 
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">
                                    <button type="submit" class="btn btn-success">Cadastrar</button>
                                </nav>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('m3.layouts.footers.auth')
    </div>
@endsection
@push('js')

@endpush