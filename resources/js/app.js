require("./bootstrap");
require("owl.carousel");
require("jquery");

$(document).ready(function () {
    $(".nav-item .no-collapse").click(function () {
        $(".drop .collapse").collapse("hide");
    });

    $(".slide").owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        nav: false,
        navText: ["<i class='fas fa-chevron-circle-right'></i>", "<i class='fas fa-chevron-circle-left'></i>"],
        lazyLoad: true,
        items: 1,
        autoplay: true,
    });

    $(".product").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left color-cyan'></i>", "<i class='fas fa-arrow-right color-cyan'></i>"],
        lazyLoad: true,
        items: 1,
        autoplay: true,
    });

    $("#blogs").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left color-cyan'></i>", "<i class='fas fa-arrow-right color-cyan'></i>"],
        lazyLoad: true,
        items: 1,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true,
            },
            600: {
                items: 1,
                nav: true,
            },
            700: {
                items: 2,
                nav: true,
            },
        },
    });

    $("#blogs-internal").owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ["<i class='fas fa-arrow-left color-cyan'></i>", "<i class='fas fa-arrow-right color-cyan'></i>"],
        lazyLoad: true,
        items: 1,
        autoplay: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true,
            },
            600: {
                items: 1,
                nav: true,
            },
            700: {
                items: 2,
                nav: true,
            },
            1000: {
                items: 3,
                nav: true,
            },
        },
    });

    $(".nav-products").owlCarousel({
        loop: false,
        nav: false,
        items: 2,
        autoplay: false,
        dots: false,
    });

    $(".nav-product").owlCarousel({
        loop: false,
        nav: true,
        items: 1,
        navText: ["<i class='fas fa-arrow-left color-cyan'></i>", "<i class='fas fa-arrow-right color-cyan'></i>"],
        autoplay: false,
        dots: false,
    });

    $("#category").on("change", function () {
        let select = $(this).find("option:selected").val();
        if (select) {
            $(".item-blog").hide();
            $("." + select).show();
        } else {
            $(".item-blog").show();
        }
    });
});
