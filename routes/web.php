<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function(){
// 	return view('auth.login');
// })->name('entrar');

Auth::routes(['register' => false, 'password.reset' => false, 'password.update' => false, 'password.email' => false]);
Route::get('/', 'HomeController@index')->name('m3');

Route::group(['prefix' => 'm3' ,'as' => 'm3.','middleware' => 'auth'], function () {

	Route::resource('usuarios', 'm3\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'm3\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'm3\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'm3\ProfileController@password']);

	Route::group(['prefix' => 'produtos' ,'as' => 'produtos.'], function () {

		Route::resource('categoria' , 'm3\CategoryProductsController',['except' => ['create','edit','update']]);
		Route::resource('lista' , 'm3\ProductsController');
		Route::resource('conteudo' , 'm3\ContentProductsController',['except' => ['show']]);

	});

	Route::group(['prefix' => 'paginas' ,'as' => 'paginas.'], function () {

		Route::resource('lista' , 'm3\PagesController',['except' => ['show']]);
		Route::resource('conteudo' , 'm3\ContentPagesController',['except' => ['show']]);
		Route::resource('categorias' , 'm3\CategoryPagesController',['except' => ['show']]);

	});
	
	Route::resource('slides' , 'm3\SlidesController',['except' => ['show']]);
	Route::resource('onde-estamos' , 'm3\MapsController',['except' => ['show']]);
	Route::resource('postagens' , 'm3\PostsController',['except' => ['show']]);
	Route::resource('configuracoes' , 'm3\SettingsController',['except' => ['show']]);
	Route::resource('galeria' , 'm3\GallerysController',['except' => ['index']]);
	Route::resource('contatos' , 'm3\ContactsController',['except' => ['create']]);
	Route::resource('newslleters' , 'm3\NewslletersController',['except' => ['create','update','edit','store']]);
	Route::resource('representantes' , 'm3\RepresentativesController',['except' => ['create','edit','store']]);

});