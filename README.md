## About Base

Essa base estar em desenvolvimento usando o Argon Admin Laravel, feito e disponibilizado pelo Tim Creative.
**[Módulos Desenvolvidos:]**

-   Contatos;
-   Newslleter;
-   Categorias de Páginas;
-   Páginas Estáticas;
-   Categorias de Produtos;
-   Produtos;
-   Slides;
-   Blogs;
-   Configurações;
-   Cadastro de Usuários;
-   Onde Estamos;

São módulo **[Defaults]** que usamos na maioria dos projetos. Assim, só basta desenvolver o front-end com essa base.

## Argon Admin Laravel

-   **[https://argon-dashboard-pro-laravel.creative-tim.com/docs/getting-started/overview.html](https://argon-dashboard-pro-laravel.creative-tim.com/docs/getting-started/overview.html) **

## Execultar projeto

1º php artisan migrate --seed
2º php artisan serve
