<?php

use Illuminate\Database\Seeder;
use App\Http\Model\Maps;

class MapsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Loja',
                'maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.1950356501115!2d-35.22256188463289!3d-5.828103259082475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2ff7b1696154d%3A0xf1c84b823a7b611b!2sNova%20M3!5e0!3m2!1spt-BR!2sbr!4v1598736940161!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'
            ],

            [
                'title' => 'Fábrica',
                'maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.1950356501115!2d-35.22256188463289!3d-5.828103259082475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2ff7b1696154d%3A0xf1c84b823a7b611b!2sNova%20M3!5e0!3m2!1spt-BR!2sbr!4v1598736940161!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'
            ],
        ];

        foreach($items as $item):
            $maps = new Maps;
            $maps->create($item);
        endforeach;
    }
}
