<?php

use Illuminate\Database\Seeder;
use App\Http\Model\TypeUsers;

class TypeUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Administrador'
            ],
            [
                'title' => 'Editor'
            ]
        ];

        foreach($items as $item){
            $type = new TypeUsers;
            $type->create($item);
        }
    }
}
