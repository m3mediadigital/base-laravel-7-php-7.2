<?php

use Illuminate\Database\Seeder;
use App\Http\Model\Pages;
use App\Http\Model\Files;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items =[
            [
                'title' => '<strong>AP</strong>FORM',
                'slug' => 'apform',
                'subtitle' => 'Fruto de um trabalho incansável',
                'imagem' => 'images/min/about-us.png',
                'content_top' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            ],
            [
                'title' => 'Em todo o <span>Brasil</span>',
                'slug' => 'em-todo-o-brasil',
                'imagem' => 'images/min/mapa.png',
                'content_top' => '<p>A APFORM atua no mercado nacional, fabricando mobiliário escolar e para escritório, além de materiais para utilidade urbana.</p>',
                'content_bottom' => '<br/>
                            <strong>Logistica</strong>
                            <br/>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>',
            ],
            [
                'title' => 'Sobre <span>Nós</span>',
                'slug' => 'sobre-nos',
                'subtitle' => 'A APFORM há 15 anos iniciou suas atividades.  Dentre as empresas similares, a APFORM foi a que alcançou a maior taxa de 
                            crescimento nesse curto período de tempo. Fruto de um trabalho incansável em busca de se ter uma participação de destaque no 
                            cenário regional.',
                'imagem' => 'images/min/about-us.png',
                'content_top' => '<p>A APFORM já conquistou prêmios de qualidade e design de produtos a nível nacional, já foi apontada como a mais inovadora 
                            no que se refere a lançamentos de produtos e gestão administrativa. E não para por aí, a APFORM é reconhecidamente a empresa na 
                            qual seus funcionários sentem-se como responsáveis por seu crescimento, participação e manutenção de seus objetivos.</p>',
                'content_bottom' => '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>',
            ],
          
        ];

        foreach($items as $item){
            $file = new Files([
                'name' => $item['imagem'],
                'filename' => $item['imagem'],
                'path' => $item['imagem']
            ]);

            $file->save();

            $page = new Pages([
                'files_id' => $file['id'],
                'title' => $item['title'],
                'subtitle' => isset($item['subtitle']) ? $item['subtitle'] : null,
                'content_top' => $item['content_top'],
                'content_bottom' => isset($item['content_bottom']) ? $item['content_bottom'] : null,
                'slug' => $item['slug'],
            ]);
            // $page->setData($page, $item);
            // $page->files_id = $file['id'];
            $page->save();
        }
    }
}
