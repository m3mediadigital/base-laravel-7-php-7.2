<?php

use Illuminate\Database\Seeder;

use App\Http\Model\Settings;

class SettingsTableSeeder extends Seeder
{
    
    public function run()
    {
        $items = [
            [
                'title' => 'E-mail Principal',
                'content' => 'producao@novam3.com'
            ],
            [
                'title' => 'E-mail Newslleter',
                'content' => 'producao@novam3.com'
            ],
            [
                'title' => 'E-mail Contato Footer',
                'content' => 'sac@apform.com.br // contato@apform.com.br'
            ],
            [
                'title' => 'Contato',
                'content' => '(84) 98802-3825 // (84) 99103-2534'
            ],
            [
                'title' => 'Whatsapp',
                'content' => '(84) 0000 0000'
            ],
            [
                'title' => 'Facebook',
                'content' => 'facebook.com'
            ],
            [
                'title' => 'Instagram',
                'content' => 'instagram.com'
            ],
            [
                'title' => 'Descrição do site',
                'content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
            ],
            [
                'title' => 'KeyWords',
                'content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
            ],
            [
                'title' => 'Google analitycs',
                'content' => ''
            ],
            [
                'title' => 'Google Tag Manage Head',
                'content' => ''
            ],
            [
                'title' => 'Google Tag Manage Body',
                'content' => ''
            ]
        ];

        foreach($items as $item){
            $setting = new Settings;
            $setting->setData($setting, $item);
            $setting->save();
        }
    }
}
