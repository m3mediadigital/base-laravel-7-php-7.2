<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([TypeUsersTableSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        $this->call([PagesTableSeeder::class]);
        $this->call([SlidesTableSeeder::class]);
        $this->call([SettingsTableSeeder::class]);
        $this->call([CategoryProductsTableSeeder::class]);
        $this->call([ProductsTableSeeder::class]);
        $this->call([MapsTableSeeder::class]);
    }
}
