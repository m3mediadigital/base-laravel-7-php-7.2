<?php

use Illuminate\Database\Seeder;

use App\Http\Model\Slides;
use App\Http\Model\Files;

class SlidesTableSeeder extends Seeder
{
    
    public function run()
    {
        $items = [
            [       
                'link' => '#',
                'position' => 1,
                'active' => 1,
                'start_at' => '01/01/2020',
                'finish_at' => '2030/12/01'
            ]
        ];

        foreach($items as $item){
            $file = new Files([
                'name' => 'images/min/slide.jpg',
                'filename' => 'images/min/slide.jpg',
                'path' => 'images/min/slide.jpg'
            ]);

            $file->save();

            $slide = new Slides;
            $slide->setData($slide, $item);
            $slide->files_id = $file['id'];
            $slide->save();
        }
    }
}
