<?php

use Illuminate\Database\Seeder;
use App\Http\Model\Files;
use App\Http\Model\Products;
use App\Http\Model\CategoryProducts;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Armário',
                'subtitle' => 'Domus Inox | Aço',
                'content' => 'O armário de aço da linha DOMUS, é a melhor escolha para seu ambiente de trabalho. 
                            Com um exclusivo design premiado, que elimina quinas vivas e cortantes, 
                            proporcionando maior resistência mecânica e rigidez estrutural além da segurança. 
                            São 4 opções de cores para seu ambiente de trabalho, e duas opções de 
                            corpo: AÇO INOX e AÇO CARBONO, sendo que este último com ensaio em laboratório, da 
                            camada de tinta e resistência a corrosão.',
                'category_products_id' => 1,
                'image' => 'images/min/product.png'
            ],
            [
                'title' => 'Armário',
                'subtitle' => 'Domus Inox | Aço',
                'content' => 'O armário de aço da linha DOMUS, é a melhor escolha para seu ambiente de trabalho. 
                            Com um exclusivo design premiado, que elimina quinas vivas e cortantes, 
                            proporcionando maior resistência mecânica e rigidez estrutural além da segurança. 
                            São 4 opções de cores para seu ambiente de trabalho, e duas opções de 
                            corpo: AÇO INOX e AÇO CARBONO, sendo que este último com ensaio em laboratório, da 
                            camada de tinta e resistência a corrosão.',
                'category_products_id' => 2,
                'image' => 'images/min/product.png'
            ],
            [
                'title' => 'Armário',
                'subtitle' => 'Domus Inox | Aço',
                'content' => 'O armário de aço da linha DOMUS, é a melhor escolha para seu ambiente de trabalho. 
                            Com um exclusivo design premiado, que elimina quinas vivas e cortantes, 
                            proporcionando maior resistência mecânica e rigidez estrutural além da segurança. 
                            São 4 opções de cores para seu ambiente de trabalho, e duas opções de 
                            corpo: AÇO INOX e AÇO CARBONO, sendo que este último com ensaio em laboratório, da 
                            camada de tinta e resistência a corrosão.',
                'category_products_id' => 4,
                'image' => 'images/min/product.png'
            ],
            [
                'title' => 'Armário',
                'subtitle' => 'Domus Inox | Aço',
                'content' => 'O armário de aço da linha DOMUS, é a melhor escolha para seu ambiente de trabalho. 
                            Com um exclusivo design premiado, que elimina quinas vivas e cortantes, 
                            proporcionando maior resistência mecânica e rigidez estrutural além da segurança. 
                            São 4 opções de cores para seu ambiente de trabalho, e duas opções de 
                            corpo: AÇO INOX e AÇO CARBONO, sendo que este último com ensaio em laboratório, da 
                            camada de tinta e resistência a corrosão.',
                'category_products_id' => 4,
                'image' => 'images/min/product.png'
            ],
        ];

        for ($i=0; $i < 32; $i++) { 
            foreach($items as $item){
                $file = new Files([
                    'name' => $item['image'],
                    'filename' => $item['image'],
                    'path' => $item['image']
                ]);

                $file->save();

                $products = new Products;
                $products->setData($products, $item);
                $products->files_id = $file->id;
                $products->save();
            }
        }
    }
}
