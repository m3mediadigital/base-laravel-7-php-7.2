<?php

use Illuminate\Database\Seeder;
use App\Http\Model\Files;
use App\Http\Model\CategoryProducts;

class CategoryProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Linha <span>FNDE</span>',
                'position' => 1,
                'image' => 'images/products/linha_fnde.jpg',
                'slug' => 'linha-fnde'
            ],
            [
                'title' => 'Linha <span>APFLUID</span>',
                'position' => 2,
                'image' => 'images/products/linha_apfluid.jpg',
                'slug' => 'linha-apfuid'
            ],
            [
                'title' => 'Linha <span>DOMUS</span>',
                'position' => 3,
                'image' => 'images/products/linha_domus.jpg',
                'slug' => 'linha-domus'
            ],
            [
                'title' => 'Linha <span>PET</span>',
                'position' => 4,
                'image' => 'images/products/linha_pet.jpg',
                'slug' => 'linha-pet'
            ],
        ];


         foreach($items as $item){
            $file = new Files([
                'name' => $item['image'],
                'filename' => $item['image'],
                'path' => $item['image']
            ]);

            $file->save();
       
            $category = new CategoryProducts([
                'title' => $item['title'],
                'position' => $item['position'],
                'slug' => $item['slug'],
                'files_id' => $file->id,
            ]);
            $category->save();
        }
    }
}
