<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('users_id')->constrained();
            $table->foreignId('files_id')->constrained()->onDelete('cascade');
            // $table->foreignId('category_pages_id')->constrained()->onDelete('cascade')->nullable();
            $table->string('title');
            $table->longText('subtitle')->nullable();
            $table->longText('content_top')->nullable();
            $table->longText('content_bottom')->nullable();
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
