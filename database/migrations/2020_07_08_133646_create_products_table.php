<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('users_id')->constrained();
            $table->foreignId('files_id')->constrained()->onDelete('cascade');
            // $table->foreignId('pages_id')->constrained()->onDelete('cascade');
            // $table->integer('index');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->longText('content');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
